FROM httpd

COPY ./apache/admin.dev2.ack.com.conf /etc/apache2/sites-available/
COPY ./apache/pim.dev2.ack.com.conf /etc/apache2/sites-available/
RUN mkdir /etc/apache2/sites-enabled 
RUN ln -s /etc/apache2/sites-available/admin.dev2.ack.com.conf /etc/apache2/sites-enabled/admin.dev2.ack.com.conf
RUN ln -s /etc/apache2/sites-available/pim.dev2.ack.com.conf /etc/apache2/sites-enabled/pim.dev2.ack.com.conf

COPY ./apache/php.ini /etc/php.ini
COPY ./apache/httpd-vhosts.conf /usr/local/apache2/conf/extra/httpd-vhosts.conf
COPY ./apache/httpd.conf /usr/local/apache2/conf/httpd.conf
COPY ./apache/.htpasswd /usr/local/.htpasswd

RUN mkdir /var/log/httpd

# SYMLINKS
#RUN ln -s /Applications/XAMPP/htdocs www
#RUN ln -s /Applications/XAMPP/etc server

#SYSTEM UPDATES
RUN apt-get update
RUN apt-get install -y unzip zip
RUN apt-get install -y --fix-missing zlib1g-dev 
RUN apt-get install -y libxml2-dev
RUN apt-get install -y libapache2-mod-proxy-html

# try these live
#RUN a2enmod proxy
#RUN a2enmod proxy_http
#RUN a2ensite admin.dev2.ack.com
#RUN a2ensite pim.dev2.ack.com

COPY ./apache/ssl/certs/server.crt /etc/ssl/certs/server.crt
#COPY ./apache/ssl/certs/server.csr /etc/ssl/certs/gd_bundle-g2-g1.crt
COPY ./apache/ssl/certs/server.key /etc/ssl/certs/server.key

#COPY ./apache/ssl/certs/2017-ack-com.crt /etc/ssl/certs/2017-ack-com.crt
#COPY ./apache/ssl/certs/gd_bundle-g2-g1.crt /etc/ssl/certs/gd_bundle-g2-g1.crt
#COPY ./apache/ssl/certs/ack-com.key /etc/ssl/certs/ack-com.key

# MYSQL
#RUN docker-php-ext-install mysql mysqli pdo pdo_mysql
#RUN apt-get install --force-yes -y php5-cli


# Set application directory
#WORKDIR /var/www/html

#PHP extentions
#RUN docker-php-ext-install gettext pdo_mysql zip soap
#RUN a2enmod rewrite
#Fix for php CLI
#RUN unlink /usr/bin/php && ln -s /usr/local/bin/php /usr/bin/php

# CONFIG
#COPY ./apache/httpd-vhosts.conf /server/extra/
#COPY ./apache/hosts /etc/hosts
#COPY ./apache/php.ini /etc/php.ini

#RUN sed 's/\/\/Include etc\/extra\/httpd-vhosts.conf\/Include etc\/extra\/httpd-vhosts.conf/'' /Applications/XAMPP/etc/httpd.conf

# actually for ecomm
#RUN sed -i 's/self::airbrakeLog/\/\/self::airbrakeLog/g' ./library/Log.php

#RUN ls -la /var/www/html
#RUN ls -la /var/www/html/ack_admin

#ENV incl "require_once($_SERVER['DOCUMENT_ROOT'] . \"/includes/setup.php\""
#ENV incl2 "//require_once($_SERVER['DOCUMENT_ROOT'] . \"/includes/setup.php\""
#RUN sed -i 's/${incl}/${incl2}/g' /var/www/html/ack_admin/_libs/admin_header.php

# ALIASES
#COPY ./.bash_profile ~
#RUN source ~/.bash_profile
