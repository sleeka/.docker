FROM mysql:5.5.44

EXPOSE 3306

# LOAD AUSKAYAK #

#COPY ./auskayak.sql /tmp/
COPY ./mysql/seeds/1init_db.sh /
#COPY ./mysql/config/5.5.44/my.cnf /etc/sysconfig/mysqld-config/
#COPY ./mysql/config/5.5.44/my.cnf /etc/mysql/

#CMD ["./1init_db.sh"]

