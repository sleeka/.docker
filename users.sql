-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 14, 2017 at 08:05 PM
-- Server version: 5.5.58-log
-- PHP Version: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ackdev`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) DEFAULT NULL,
  `UserPassword` varchar(50) DEFAULT NULL,
  `password_encrypted` blob NOT NULL,
  `UserIsAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `UserLevel` int(1) NOT NULL,
  `userBatch` tinyint(1) DEFAULT '0',
  `UserIP` tinyint(1) DEFAULT NULL,
  `UserIsPartner` tinyint(1) DEFAULT NULL,
  `Deleted` char(1) NOT NULL DEFAULT 'N',
  `ClassLevel` int(1) NOT NULL DEFAULT '0',
  `user_type` varchar(12) DEFAULT NULL,
  `user_first` varchar(75) NOT NULL,
  `user_last` varchar(75) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_phone` varchar(20) NOT NULL,
  `last_login` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(4) NOT NULL,
  `title` varchar(75) NOT NULL,
  `department` varchar(75) NOT NULL,
  `phone_office` varchar(15) NOT NULL,
  `phone_office_ext` varchar(10) NOT NULL,
  `em_name` varchar(75) NOT NULL,
  `em_phone` varchar(25) NOT NULL,
  `em_email` varchar(75) NOT NULL,
  `v2` tinyint(4) NOT NULL DEFAULT '0',
  `employee_number` smallint(3) DEFAULT NULL,
  `auth_token` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  KEY `UserName` (`UserName`),
  KEY `employee_number` (`employee_number`),
  KEY `employee_number_2` (`employee_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=707 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `UserName`, `UserPassword`, `password_encrypted`, `UserIsAdmin`, `UserLevel`, `userBatch`, `UserIP`, `UserIsPartner`, `Deleted`, `ClassLevel`, `user_type`, `user_first`, `user_last`, `user_email`, `user_phone`, `last_login`, `modified`, `is_active`, `title`, `department`, `phone_office`, `phone_office_ext`, `em_name`, `em_phone`, `em_email`, `v2`, `employee_number`, `auth_token`, `remember_token`) VALUES
(706, 'sean_leeka', NULL, 0x168a6bb200697e2f52f778b6d9272d268596afc441cad07cbf28c92e4fe6d4e6, 1, 1, 1, 1, NULL, 'N', 0, 'std', 'Sean', 'Leeka', 'sleeka@swbell.net', '2108549824', '2017-11-14 17:52:19', '2017-09-26 15:58:12', 0, 'Web Developer', 'Development', '2108549824', '', '', '', '', 0, NULL, 'efcb43d7948e832147309ebfb09a691935133d99de93791134921b674966ef41', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
