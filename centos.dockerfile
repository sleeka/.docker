FROM centos:centos7

RUN yum install -y git wget libxml2 libxml2-devel openssl bzip2-devel libcurl-devel libjpeg-devel libpng-devel freetype-devel epel-release cron
RUN rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm http://rpms.remirepo.net/enterprise/remi-release-7.rpm
RUN yum --enablerepo=remi install php72-php -y
RUN yum install -y libc-client uw-imap uw-imap-devel uw-imap-static uw-imap-utils php72w-php-common php72w-composer php-zip unzip yum-utils supervisor
RUN yum groupinstall "Development tools" -y
RUN yum update -y

RUN mkdir /var/spool/cron
RUN touch /var/spool/cron/root
RUN echo "0 0 * * * touch /var/www/html/admin/storage/logs/laravel-$(date +\%Y-\%m-\%d).log && chown nginx:nginx /var/www/html/admin/storage/logs/laravel-$(date +\%Y-\%m-\%d).log && chmod 777 /var/www/html/admin/storage/logs/laravel-$(date +\%Y-\%m-\%d).log >> /dev/null 2>&1" > /var/spool/cron/root
RUN echo "* * * * * USE_ZEND_ALLOC=0 /usr/bin/php /var/www/html/admin/artisan schedule:run >> /dev/null 2>&1" >> /var/spool/cron/root
RUN echo "\n" >> /var/spool/cron/root

WORKDIR /var/www/html/admin_v3
COPY ./php.ini /etc/php.ini
COPY ./php-fpm.conf.etc /etc/php-fpm.conf
COPY ./php-fpm.conf.nginx /etc/nginx/conf.d/php-fpm.conf
COPY ./laravel-worker.conf.ini /etc/supervisor.d/laravel-worker.conf.ini


################## ALL COMMENTED:    (helpful old work)

#RUN git clone https://github.com/php/php-src.git /tmp/php-src
#RUN cd /tmp/php-src && git checkout PHP-7.2 && ./buildconf --force && ./configure --enable-bcmath --with-bz2 --enable-calendar --with-curl --enable-exif --enable-ftp --with-gd --with-jpeg-dir --with-png-dir --with-freetype-dir --enable-gd-native-ttf --with-imap --with-imap-ssl --with-kerberos --enable-mbstring --with-mcrypt --with-mhash --with-mysql --with-mysqli --with-openssl --with-pcre-regex --with-pdo-mysql --with-zlib-dir --with-regex --enable-sysvsem --enable-sysvshm --enable-sysvmsg --enable-soap --enable-sockets --with-xmlrpc --enable-zip --with-zlib --enable-inline-optimization --enable-mbregex --enable-opcache --enable-fpm --with-fpm-user=apache --with-fpm-group=nginx --with-libdir=lib64 --prefix=/usr/local --with-config-file-path=/etc && make && make install




### install php: ####
#RUN wget -q http://rpms.remirepo.net/enterprise/remi-release-7.rpm
#RUN wget -q https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# Might just need this; if EPEL is enabled, neither will activate below
#RUN rpm -Uvh remi-release-7.rpm
#RUN rpm -Uvh remi-release-7.rpm epel-release-latest-7.noarch.rpm
#RUN yum-config-manager --enable remi-php70

#RUN yum install -y php
#####################

#RUN yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
#                https://rpms.remirepo.net/enterprise/remi-release-7.rpm \
#                bzip2-devel curl-devel libjpeg-devel git \
#				libpng-devel freetype-devel libc-client \
#				libc-client-devel libmcrypt-devel wget \
#				autoconf bison re2c gcc openssl-devel \
#				libxml2-devel libc-client libc-client-devel \
#				openssl pam-devel uw-imap-devel uw-imap php-imap -y
#RUN ls -la /usr/local/ssl
#RUN yum install -y libkrb5-dev && rm -r /var/lib/

#RUN git clone https://github.com/php/php-src.git && 
#ADD ./php-src /tmp/php-src

#RUN cd /tmp/php-src && git checkout PHP-7.2 && ./buildconf --force && ./configure --enable-bcmath --with-bz2 --enable-calendar --with-curl --enable-exif 


#RUN php -i

## PHP ##
#########
#RUN wget -q http://rpms.remirepo.net/enterprise/remi-release-7.rpm
#RUN wget -q https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

#RUN rpm -Uvh remi-release-7.rpm epel-release-latest-7.noarch.rpm

# 70 or 71 works
#RUN yum-config-manager --enable remi-php72


#RUN yum install -y php
#########
## PHP ##

#RUN yum install -y php72-php-pdo pdo_mysql gd php72-php-mbstring php-mbstring mysqli bcmath calendar php-soap php-pdo gdb dos2unix php-simplexml php-zip unzip vim composer

# Useful for gdb; also type in gdp prompt: set disable-randomization off
#RUN debuginfo-install php-cli-7.2.0-2.el7.remi.x86_64 bzip2-libs-1.0.6-13.el7.x86_64 cyrus-sasl-lib-2.1.26-21.el7.x86_64 keyutils-libs-1.5.8-3.el7.x86_64 libcurl-7.29.0-42.el7_4.1.x86_64 libgcrypt-1.5.3-14.el7.x86_64 libgpg-error-1.12-3.el7.x86_64 libidn-1.28-4.el7.x86_64 libselinux-2.5-11.el7.x86_64 libssh2-1.4.3-10.el7_2.1.x86_64 libxslt-1.1.28-5.el7.x86_64 nspr-4.13.1-1.0.el7_3.x86_64 nss-3.28.4-15.el7_4.x86_64 nss-softokn-freebl-3.28.3-8.el7_4.x86_64 nss-util-3.28.4-3.el7.x86_64 openldap-2.4.44-5.el7.x86_64 pcre-8.32-17.el7.x86_64 sqlite-3.7.17-8.el7.x86_64 xz-libs-5.2.2-1.el7.x86_64 -y

#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


## order?
#RUN yum install -y libxml2-devel
#ENTRYPOINT ["/bin/bash"]


### switch from root user to apache
###################################

#COPY ./create_apache_user.sh /tmp
#RUN chmod 755 /tmp/create_apache_user.sh

#RUN yum update -y

#RUN dos2unix /tmp/create_apache_user.sh
#RUN sed -i 's/\r//' /tmp/create_apache_user.sh
#RUN sed -i 's/^#! \/bin\/sh/#! \/bin\/bash/' /tmp/create_apache_user.sh
#RUN echo "\r" >> /tmp/create_apache_user.sh
#CMD ["/tmp/create_apache_user.sh"]

#RUN chsh -s /bin/bash apache

#USER apache
WORKDIR /var/www/html/admin_v3

#RUN ls -la
#RUN composer install


#RUN yum install apache2 -y
#RUN a2enmod proxy
#RUN a2enmod proxy_http

#COPY ./apache/admin.dev2.ack.com.conf /etc/apache2/sites-available
#COPY ./apache/pim.dev2.ack.com.conf /etc/apache2/sites-available

#RUN service apache2 restart

# Labels consumed by Red Hat build service
#LABEL Component="nginx" \
#		Name="centos/nginx-180-centos7" \
#		Version="1.8.0" \
#		Release="1"

# Labels could be consumed by OpenShift
#LABEL io.k8s.description="nginx [engine x] is an HTTP and reverse proxy server, a mail proxy server, and a generic TCP/UDP proxy server, originally written by Igor Sysoev." \
#      io.k8s.display-name="nginx 1.8.0" \
#      io.openshift.expose-services="80:http" \
#      io.openshift.tags="nginx"

#RUN yum -y install --setopt=tsflags=nodocs centos-release-scl-rh && \
#    yum -y update --setopt=tsflags=nodocs && \
#    yum -y install --setopt=tsflags=nodocs scl-utils rh-nginx18 && \
#    yum clean all && \
#    mkdir -p /usr/share/nginx/html

# Get prefix path and path to scripts rather than hard-code them in scripts
#ENV CONTAINER_SCRIPTS_PATH=/usr/share/container-scripts/nginx \
#ENABLED_COLLECTIONS=rh-nginx18

# When bash is started non-interactively, to run a shell script, for example it
# looks for this variable and source the content of this file. This will enable
# the SCL for all scripts without need to do 'scl enable'.
#ENV BASH_ENV=${CONTAINER_SCRIPTS_PATH}/scl_enable \
#    ENV=${CONTAINER_SCRIPTS_PATH}/scl_enable \
#    PROMPT_COMMAND=". ${CONTAINER_SCRIPTS_PATH}/scl_enable"

# SYMLINKS
#RUN ln -s /Applications/XAMPP/htdocs www
#RUN ln -s /Applications/XAMPP/etc server

# CONFIG
#COPY ./apache/httpd-vhosts.conf /server/extra/
#COPY ./apache/hosts /private/etc/hosts
#COPY ./apache/php.ini /Applications/XAMPP/etc/php.ini

#RUN sed 's/\/\/Include etc\/extra\/httpd-vhosts.conf\/Include etc\/extra\/httpd-vhosts.conf/'' /Applications/XAMPP/etc/httpd.conf

#RUN sed 's/self::airbrakeLog($message, $priority);/\/\/self::airbrakeLog($message, $priority);/' ./ack_common/library/Log.php

# ALIASES
#COPY ./.bash_profile ~
#RUN source ~/.bash_profile

# SYSTEM UPDATES

# MYSQL
#RUN docker-php-ext-install mysql mysqli pdo pdo_mysql
#RUN apt-get install --force-yes -y php5-cli

# PHP EXTENSIONS
