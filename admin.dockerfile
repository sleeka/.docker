#FROM yutaf/php-5.3.3
#FROM php:5-apache
#FROM eboraas/apache-php
#FROM tomsowerby/php-5.3
#FROM shanderlam/php5.3-fpm

FROM orsolin/docker-php-5.3-apache

#SYSTEM UPDATES
RUN apt-get update
RUN apt-get install -y unzip zip nano
RUN apt-get install -y --fix-missing zlib1g-dev 
RUN apt-get install -y libxml2-dev
RUN apt-get install apache2 -y

COPY ./admin/admin.dev2.ack.com.conf /etc/apache2/sites-available/
COPY ./admin/pim.dev2.ack.com.conf /etc/apache2/sites-available/
COPY ./admin/ack.com.conf /etc/apache2/sites-available/

# REMOVE .conf ?
#RUN a2ensite admin.dev2.ack.com.conf
#RUN a2ensite pim.dev2.ack.com.conf
RUN a2ensite ack.com


#RUN ln -s /etc/apache2/sites-available/admin.dev2.ack.com.conf /etc/apache2/sites-enabled/admin.dev2.ack.com.conf
#RUN ln -s /etc/apache2/sites-available/pim.dev2.ack.com.conf /etc/apache2/sites-enabled/pim.dev2.ack.com.conf

#COPY ./admin/php.ini /etc/php.ini
#COPY ./admin/httpd-vhosts.conf /usr/local/apache2/conf/extra/httpd-vhosts.conf


#COPY ./admin/httpd.conf /usr/local/apache2/conf/httpd.conf

COPY ./admin/dev1/conf/httpd.conf /usr/local/apache2/conf/httpd.conf
RUN mkdir /etc/apache2/conf.d
COPY ./admin/conf.d/* /etc/apache2/conf.d/

COPY ./admin/.htaccess /var/www/html/ack_admin/
COPY ./admin/.htpasswd /usr/local/.htpasswd
COPY ./admin/ssl/certs/server.crt /etc/ssl/certs/server.crt
COPY ./admin/ssl/certs/server.key /etc/ssl/certs/server.key
COPY ./admin/ports.conf /etc/apache2/ports.conf
COPY ./admin/apache2.conf /etc/apache2/apache2.conf
COPY ./admin/my.cnf /etc/mysql/

#RUN touch /var/log/apache2/admin.dev2.ack.com-access.log
#RUN touch /var/log/apache2/pim.dev2.ack.com-error.log
#RUN touch /var/log/apache2/admin.dev2.ack.com-access-443.log 
#RUN touch /var/log/apache2/pim.dev2.ack.com-error-443.log 
#
#RUN chmod 664 /var/log/apache2/admin.dev2.ack.com-access.log
#RUN chmod 664 /var/log/apache2/pim.dev2.ack.com-error.log
#RUN chmod 664 /var/log/apache2/admin.dev2.ack.com-access-443.log 
#RUN chmod 664 /var/log/apache2/pim.dev2.ack.com-error-443.log 
#
#RUN chown www-data /var/log/apache2/admin.dev2.ack.com-access.log
#RUN chown www-data /var/log/apache2/pim.dev2.ack.com-error.log
#RUN chown www-data /var/log/apache2/admin.dev2.ack.com-access-443.log 
#RUN chown www-data /var/log/apache2/pim.dev2.ack.com-error-443.log 


#################################################
# MYSQL
RUN docker-php-ext-install mysql mysqli pdo pdo_mysql
RUN apt-get install --force-yes -y php5-cli
RUN docker-php-ext-install pdo_mysql soap
RUN a2enmod rewrite

#Setup MYSQL for the webserver
ENV DEBIAN_FRONTEND noninteractive
RUN bash -c 'debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_ROOT_PASSWORD"'
RUN bash -c 'debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQl_ROOT_PASSWORD"'
#RUN apt-get update
RUN apt-get -y install mysql-server


COPY ./mysql/config/5.5.44/my.cnf /etc/mysql/

#RUN apt-get install apache2 php5 -y
#RUN echo "<?php phpinfo(); ?>" >> /var/www/html/info.php

# SYMLINKS
#RUN ln -s /Applications/XAMPP/htdocs www
#RUN ln -s /Applications/XAMPP/etc server

# CONFIG
#COPY ./apache/httpd-vhosts.conf /server/extra/
#RUN if [! -d "/private/etc"]; then mkdir /private/etc; fi
#COPY ./apache/hosts /private/etc/hosts


#COPY ./apache/php.ini /etc/php.ini
###################################################

#COPY ./admin/httpd.conf /etc/apache2/httpd.conf
#COPY ./admin/www.conf /etc/php5/fpm/pool.d/www.conf

#RUN echo "#!/bin/bash\nsudo service apache2 start; php5-fpm" >> /tmp/init.sh
#RUN chmod +x /tmp/init.sh

#ENTRYPOINT ["/tmp/init.sh"]

#RUN sed 's/\/\/Include etc\/extra\/httpd-vhosts.conf\/Include etc\/extra\/httpd-vhosts.conf/'' /Applications/XAMPP/etc/httpd.conf

# actually for ecomm
#RUN sed -i 's/self::airbrakeLog/\/\/self::airbrakeLog/g' ./library/Log.php

#ENV incl "require_once($_SERVER['DOCUMENT_ROOT'] . \"/includes/setup.php\""
#ENV incl2 "//require_once($_SERVER['DOCUMENT_ROOT'] . \"/includes/setup.php\""
#RUN sed -i 's/${incl}/${incl2}/g' /var/www/html/ack_admin/_libs/admin_header.php

COPY ./admin/_db.php /var/www/html/ack_admin/_libs

# ALIASES
#COPY ./.bash_profile ~
#RUN source ~/.bash_profile
