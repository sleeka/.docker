FROM php:7.0.25-fpm











# SYMLINKS
#RUN ln -s /Applications/XAMPP/htdocs www
#RUN ln -s /Applications/XAMPP/etc server

# CONFIG
#COPY ./apache/httpd-vhosts.conf /server/extra/
#RUN if [! -d "/private/etc"]; then mkdir /private/etc; fi
#COPY ./apache/hosts /private/etc/hosts
#COPY ./apache/php.ini /etc/php.ini

# ALIASES
#COPY ./.bash_profile ~
#RUN source ~/.bash_profile

# SYSTEM UPDATES

# MYSQL
#RUN docker-php-ext-install mysql mysqli pdo pdo_mysql
#RUN apt-get install --force-yes -y

# PHP EXTENSIONS

RUN yum update -y
RUN yum update -y
