# Files needed to build a docker image and run Summit Sports' ACK admin_v3

1. Download docker from docker.com -- install and start the docker daemon

2. The .docker repo needs to sit in the same directory with all other ACK repos -- admin_v3 (erp/pim) in this case.

3. From a terminal (in Windows, use powershell) in the .docker repo, 'docker-compose build --no-cache && docker-compose up' (semicolon in Windows, not '&&')

4. Access the admin_v3 container with 'docker ps' then 'docker exec -it [container id found via docker ps] /bin/bash'


